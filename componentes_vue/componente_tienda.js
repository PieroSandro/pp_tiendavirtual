Vue.component('componente_tienda', {
    template:/*html */
    `<div>
    <div class="container">
   
<section v-show="esTienda">
<div class="row" v-for="(producto, index) in productos" style="display: inline-block;">
    <div class="col-md-12" style="margin-left: 60px;">
       <img :src="producto.img" class="img-fluid" @click="verDetalle(index)" alt=""> <br>
        <b>Nombre: </b> {{ producto.nombre }} <br>
        <b>Precio</b> {{ producto.precio }} <br>
       <b>Fecha: </b> {{ str_fecha }}  <br>
       <!--STOCK--->
  <v-container fluid>
    <v-row align="center">
      <v-col
        class="d-flex"
        cols="12"
        sm="6"
      >
  <v-select
  v-model="producto.cantidad"
  :items="obtenerStockarray(producto.stock)"
  label="Cantidad"
></v-select>
</v-col></v-row></v-container>
<!--</ul>-->
       <!--STOCK--->
       <template v-if="producto.escogido == false ">
           <button class="btn btn-danger btn-sm" @click="escoger_producto(index)">escoger</button>
        </template>
        <template v-if="producto.escogido == true ">
        <button class="btn btn-success btn-sm" @click="quitar_producto(index)">escogido</button>
     </template>
         <hr>
    </div>
</div><!-- FIN DEL V_FOR -->

</section>

<!---------NO ES TIENDA---------------------------------------------------->
<section v-show="!esTienda">
<div>
      soy el componente detalle<br>
      {{detalle_id}}<br>
      {{detalle_nombre}}<br>
      <!-- stock-->
       Cantidad: {{detalle_cantidad}}<br>




       
  <v-container fluid>
  <v-row align="center">
    <v-col
      class="d-flex"
      cols="12"
      sm="6"
    >
<v-select
v-model="detalle_cantidad"
:items="obtenerStockarray(detalle_stock)"
label="Cantidad"
></v-select>
</v-col></v-row></v-container>
        <!-- stock-->
      <template v-if="detalle_escogido == false ">
      <button class="btn btn-danger btn-sm" @click="escoger_producto(detalle_index)">escoger</button>
   </template>
   <template v-if="detalle_escogido == true ">
   <button class="btn btn-success btn-sm" @click="quitar_producto(detalle_index)">escogido</button>
</template>
  
      <button @click="volver(detalle_index)">volver</button>
    </div>
</section>
<!---------NO ES TIENDA---------------------------------------------------->
 

</div>
    </div>`,

    data(){
        return{
            str_fecha:'',
            productos:[],

            esTienda:true,
            detalle_id:0,
            detalle_nombre:'',
            detalle_escogido:false,

            detalle_index:0,
            
            productos_acumulados:[],
         //--stock-----------------
           detalle_cantidad:0,
           detalle_stock:0,
         //--stock-----------------
            
        }
    },

    methods: {
        mostrar_data(){
            axios.get('./bd/productos_tienda.json').then((response) => {
                this.productos=response.data;
             
             })
             .catch((e) => {
                 console.log(e);
             })
        },
        fechaPedido(){//OBTENIENDO LA FECHA ACTUAL DEL PEDIDO
            var fecha_pedido = new Date();
            this.str_fecha = `${fecha_pedido.getDate()} / ${fecha_pedido.getMonth() + 1} / ${fecha_pedido.getFullYear()}`;
        },
        escoger_producto(index){
            this.productos[index].escogido=!this.productos[index].escogido;
            this.detalle_escogido=this.productos[index].escogido;
            console.log("ID: "+this.productos[index].id+". Nombre: "+this.productos[index].nombre+". Escogido: "+this.productos[index].escogido);
         //-------------------------------------------------------------
            if(this.esTienda==true){ 
            var producto_seleccionado={
                "id":this.productos[index].id,
                "img":this.productos[index].img,
                "img2":this.productos[index].img2,
                "img3":this.productos[index].img3,
                "nombre":this.productos[index].nombre,
                "precio":this.productos[index].precio,
                "cantidad": this.productos[index].cantidad,
                "descripcion": this.productos[index].descripcion,
                "escogido":this.productos[index].escogido,
                "fecha": this.productos[index].fecha
        
            };
        }else{
            var producto_seleccionado={
                "id":this.productos[index].id,
                "img":this.productos[index].img,
                "img2":this.productos[index].img2,
                "img3":this.productos[index].img3,
                "nombre":this.productos[index].nombre,
                "precio":this.productos[index].precio,
                "cantidad": this.detalle_cantidad,
                "descripcion": this.productos[index].descripcion,
                "escogido":this.productos[index].escogido,
                "fecha": this.productos[index].fecha
        
            };
        }
            //----------------------------------------------------------
            this.productos_acumulados.push(producto_seleccionado);
            console.log(this.productos_acumulados);
            //console.log(producto_seleccionado);
        },
        quitar_producto(index){
            this.productos[index].escogido=!this.productos[index].escogido;
            this.detalle_escogido=this.productos[index].escogido;
            console.log("ID: "+this.productos[index].id+". Nombre: "+this.productos[index].nombre+". Escogido: "+this.productos[index].escogido);
            //---------------------------
          
           for(var i=0;i<this.productos_acumulados.length;i++){
               if(this.productos[index].id==this.productos_acumulados[i].id){
                this.productos_acumulados.splice(i, 1);
               }else{

               }
           }
           console.log(this.productos_acumulados);
            //---------------------------
        },
        verDetalle(index){
            this.esTienda=!this.esTienda;
            this.detalle_id=this.productos[index].id;
            this.detalle_nombre=this.productos[index].nombre;
            this.detalle_escogido=this.productos[index].escogido;
            //---stock-----------------
            this.detalle_cantidad=this.productos[index].cantidad;
            this.detalle_stock=this.productos[index].stock;
            //---stock-----------------
            this.detalle_index=index;
        },
        volver(detalle_index){
            this.esTienda=!this.esTienda;
           this.productos[detalle_index].cantidad=this.detalle_cantidad;
        }
        //----------24nov--------------------------
        ,
        obtenerStockarray(number){
            var numberArray = [];

            for(var i = 1; i <= number; i++){
        numberArray.push(i);
}
return numberArray;
        }
        //----------24nov--------------------------
    },
  
    created() {
       
       this.mostrar_data();
       this.fechaPedido();
    },
    
});
